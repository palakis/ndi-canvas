#ifndef NDISENDER_H
#define NDISENDER_H

#include <QObject>
#include <QImage>
#include <QThread>
#include <Processing.NDI.Lib.h>

class NDISender : public QThread
{
    Q_OBJECT
public:
    explicit NDISender(QString sourceName, QImage *img, QObject *parent = 0);
    ~NDISender();

private:
    int _framerate;
    QImage *_img;
    NDIlib_send_instance_t sendInstance;
    void SendVideoFrame(QImage *img);
    void run();
};

#endif // NDISENDER_H
