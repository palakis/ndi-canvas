#ifndef NDIFINDER_H
#define NDIFINDER_H

#include <QObject>
#include <QList>
#include <QString>
#include <Processing.NDI.Lib.h>

class NDIFinder : public QObject
{
    Q_OBJECT
public:
    explicit NDIFinder(QObject *parent = 0);
    ~NDIFinder();
    QList<NDIlib_source_t> FindSources();

private:
    NDIlib_find_instance_t findInstance;
};

#endif // NDIFINDER_H
