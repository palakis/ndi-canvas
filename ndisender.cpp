#include "ndisender.h"
#include <QtConcurrent/QtConcurrent>

NDISender::NDISender(QString sourceName, QImage* img, QObject *parent) : QThread(parent)
{   
    _img = img;
    _framerate = 60;

    QByteArray nameData = sourceName.toUtf8();

    NDIlib_send_create_t sendDesc = {0};
    sendDesc.p_ndi_name = nameData.constData();
    sendDesc.p_groups = NULL;
    sendDesc.clock_video = true;
    sendDesc.clock_audio = false;

    sendInstance = NDIlib_send_create(&sendDesc);
}

NDISender::~NDISender()
{
    NDIlib_send_destroy(sendInstance);
}

void NDISender::run()
{
    while(true) {
        // Frame clocking is done automatically
        SendVideoFrame(_img);
    }
}

void NDISender::SendVideoFrame(QImage *img)
{
    NDIlib_video_frame_t videoFrame = {0};
    videoFrame.xres = img->width();
    videoFrame.yres = img->height();
    videoFrame.FourCC = NDIlib_FourCC_type_RGBA;
    videoFrame.frame_rate_N = _framerate;
    videoFrame.frame_rate_D = 1;
    videoFrame.picture_aspect_ratio = (float)videoFrame.xres / (float)videoFrame.yres;
    videoFrame.frame_format_type = NDIlib_frame_format_type_progressive;

    videoFrame.timecode = NDIlib_send_timecode_synthesize;

    videoFrame.p_data = img->bits();
    videoFrame.line_stride_in_bytes = img->bytesPerLine();

    NDIlib_send_send_video(sendInstance, &videoFrame);
}
